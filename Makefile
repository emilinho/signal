SHELL=/bin/bash

SRC_DIR=src
BIN_DIR=bin

CC=cc
#CFLAGS=-Wall --pedantic
LDFLAGS=

.PHONY:	clean all

all:
	find $(SRC_DIR) -type f -name '*.c' | \
	xargs basename -s '.c' | \
	xargs -r -t $(MAKE)

clean:
	find $(BIN_DIR) -type f -executable -print0 | \
	xargs -0 -r rm -v

edit:
	vim $(SRC_DIR)/*.c

%: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(BIN_DIR)/$@ $<

